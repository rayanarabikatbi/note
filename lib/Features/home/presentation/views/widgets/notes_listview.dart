import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:note/Features/home/presentation/view_model/cubit/notes_cubit.dart';
import 'package:note/Features/home/presentation/views/widgets/card_note.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class NotesListView extends StatelessWidget {
  const NotesListView({super.key});

  @override
  Widget build(BuildContext context) {
    //List<NoteModel> notes = BlocProvider.of<NotesCubit>(context).notes ?? [];
    return BlocBuilder<NotesCubit, NotesState>(builder: (context, state) {
      if (state is NotesSuccess) {
        return state.note.isEmpty
            ? const Center(child: Text('No Notes Yet'))
            : ListView.builder(
                physics: const NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: state.note.length,
                itemBuilder: (context, index) {
                  return Dismissible(
                    onDismissed: (DismissDirection direction) {
                      direction = DismissDirection.startToEnd;
                    },
                    background: Container(
                      color: Colors.red,
                      child: const Icon(
                        Icons.delete,
                        color: Colors.white,
                      ),
                    ),
                    key: ObjectKey(state.note[index].id.toString()),
                    child: CardNote(
                      note: state.note[index],
                    ),
                  );
                });
      } else if (state is NotesFailure) {
        return Text(state.errMessage);
      }
      return const Center(
        child: CircularProgressIndicator(),
      );
    });
  }
}
