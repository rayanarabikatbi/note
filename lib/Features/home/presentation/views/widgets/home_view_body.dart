import 'package:flutter/material.dart';
import 'package:note/Features/home/presentation/views/widgets/notes_listview.dart';

class HomeViewBody extends StatelessWidget {
  const HomeViewBody({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Column(
            children: const [
              NotesListView(),
            ],
          ),
        ),
      ),
    );
  }
}
