import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:note/Features/home/data/models/note_model.dart';
import 'package:note/Features/home/presentation/view_model/cubit/notes_cubit.dart';

class CardNote extends StatelessWidget {
  const CardNote({
    Key? key,
    required this.note,
  }) : super(key: key);

  final NoteModel note;
  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        title: Text(note.title),
        subtitle: Text(note.description),
        trailing: IconButton(
          onPressed: () {
            BlocProvider.of<NotesCubit>(context).deletNote(note.id);
            BlocProvider.of<NotesCubit>(context).fetchAllNotes();
          },
          icon: const Icon(
            Icons.delete,
            color: Colors.red,
          ),
        ),
      ),
    );
  }
}
