import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:note/Features/home/data/models/note_model.dart';
import 'package:note/Features/home/presentation/view_model/cubit/notes_cubit.dart';
import 'package:note/core/notifications.dart';
import 'package:note/core/widget/custom_textfield.dart';

class AddNoteView extends StatefulWidget {
  const AddNoteView({super.key});

  @override
  State<AddNoteView> createState() => _AddNoteViewState();
}

class _AddNoteViewState extends State<AddNoteView> {
  String title = 'dododo', subTitle = 'jojo';
  final GlobalKey<FormState> formKey = GlobalKey();
  final titleController = TextEditingController();
  final descConrtroller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade200,
      appBar: AppBar(
        backgroundColor: Colors.orange,
        title: const Text('Add Note'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Form(
            key: formKey,
            child: Column(
              children: [
                CustomTextField(
                  controller: titleController,
                  hintText: 'Add Title note',
                  onSaved: (value) {
                    title = value!;
                  },
                ),
                const SizedBox(
                  height: 10,
                ),
                CustomTextField(
                  controller: descConrtroller,
                  hintText: 'Add Description ',
                  maxLine: 10,
                  onSaved: (value) {
                    subTitle = value!;
                  },
                ),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.orange,
                  ),
                  onPressed: () async {
                    NotificationWeekAndTime? pickedSchedule =
                        await pickSchedule(context);

                    if (pickedSchedule != null) {
                      createReminderNotification(
                        pickedSchedule,
                        titleController.text,
                        descConrtroller.text,
                      );
                    }
                  },
                  child: const Text('pick a date'),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.orange,
                    ),
                    onPressed: () {
                      if (formKey.currentState!.validate()) {
                        formKey.currentState!.save();
                        NoteModel noteModel = NoteModel(
                          title: title,
                          description: subTitle,
                        );
                        context.read<NotesCubit>().addNote(noteModel);
                        GoRouter.of(context).pop();
                      }
                    },
                    child: const Text('Add Note'),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
