import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive/hive.dart';
import 'package:note/Features/home/data/models/note_model.dart';
import 'package:note/core/constants.dart';

part 'notes_state.dart';

class NotesCubit extends Cubit<NotesState> {
  NotesCubit() : super(NotesInitial());
  List<NoteModel> notes = [];

  fetchAllNotes() async {
    var noteBox = Hive.box<NoteModel>(Constant.kNoteBox);
    notes = noteBox.values.toList();
    emit(NotesSuccess(notes));
  }

  addNote(NoteModel note) async {
    emit(NotesLoading());
    try {
      var noteBox = Hive.box<NoteModel>(Constant.kNoteBox);
      await noteBox.add(note);
      notes.add(note);
      emit(NotesSuccess(notes));
    } catch (e) {
      emit(NotesFailure(e.toString()));
    }
  }

  Future deletNote(int id) async {
    var noteBox = Hive.box<NoteModel>(Constant.kNoteBox);
    notes.removeAt(id);
    await noteBox.deleteAt(id);
  }
}
