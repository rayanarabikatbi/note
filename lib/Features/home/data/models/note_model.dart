import 'package:hive_flutter/hive_flutter.dart';

part 'note_model.g.dart';

@HiveType(typeId: 0)
class NoteModel extends HiveObject {
  @HiveField(0, defaultValue: 0)
  int id;
  @HiveField(1)
  String title;
  @HiveField(2)
  String description;

  NoteModel({
    this.id = 0,
    required this.title,
    required this.description,
  });
}
