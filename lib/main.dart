import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:note/Features/home/data/models/note_model.dart';
import 'package:note/Features/home/presentation/view_model/cubit/notes_cubit.dart';
import 'package:note/core/constants.dart';
import 'package:note/core/utils/app_router.dart';

void main() async {
  await Hive.initFlutter();
  Hive.registerAdapter(NoteModelAdapter());

  // await Hive.deleteBoxFromDisk(Constant.kNoteBox);
  await Hive.openBox<NoteModel>(Constant.kNoteBox);
  WidgetsFlutterBinding.ensureInitialized();

  AwesomeNotifications().initialize(null, [
    NotificationChannel(
      channelKey: 'scheduled_channel',
      channelName: 'Scheduled Notifications',
      defaultColor: Colors.orange,
      channelDescription: 'description_channel',
      // locked: true,
      importance: NotificationImportance.High,
      playSound: true,
    ),
  ]);

  runApp(const NoteApp());
}

class NoteApp extends StatelessWidget {
  const NoteApp({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => NotesCubit()..fetchAllNotes(),
      child: MaterialApp.router(
        debugShowCheckedModeBanner: false,
        routerConfig: AppRouter.router,
      ),
    );
  }
}
