import 'package:go_router/go_router.dart';
import 'package:note/Features/home/presentation/views/add_note_view.dart';
import 'package:note/Features/home/presentation/views/home_view.dart';

abstract class AppRouter {
  static const kAddNote = '/AddNote';
  static final router = GoRouter(routes: [
    GoRoute(
      path: '/',
      builder: (context, state) => const HomeView(),
    ),
    GoRoute(
      path: kAddNote,
      builder: (context, state) => const AddNoteView(),
    ),
  ]);
}
