import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  const CustomTextField({
    Key? key,
    required this.hintText,
    this.maxLine = 1,
    this.onSaved,
    this.onChanged,
    this.keyboardType,
    this.expands = false,
    required this.controller,
  }) : super(key: key);

  final String hintText;
  final int? maxLine;
  final void Function(String?)? onSaved;
  final Function(String)? onChanged;
  final TextInputType? keyboardType;
  final bool expands;
  final TextEditingController controller;
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      expands: expands,
      keyboardType: keyboardType,
      onChanged: onChanged,
      onSaved: onSaved,
      validator: (value) {
        if (value?.isEmpty ?? true) {
          return "Field is required";
        } else {
          return null;
        }
      },
      maxLines: maxLine,
      decoration: InputDecoration(
        hintText: hintText,
        border: buildBorder(),
        enabledBorder: buildBorder(),
        focusedBorder: buildBorder(),
      ),
      cursorColor: Colors.orange,
    );
  }

  OutlineInputBorder buildBorder([color]) {
    return OutlineInputBorder(
      borderRadius: BorderRadius.circular(8),
      borderSide: BorderSide(
        color: color ?? Colors.orange,
      ),
    );
  }
}
